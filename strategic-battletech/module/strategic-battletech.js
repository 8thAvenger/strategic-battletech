// Import Modules
import { strategicbattletech.jsActor } from "./actor/actor.js";
import { strategicbattletech.jsActorSheet } from "./actor/actor-sheet.js";
import { strategicbattletech.jsItem } from "./item/item.js";
import { strategicbattletech.jsItemSheet } from "./item/item-sheet.js";

Hooks.once('init', async function() {

  game.strategic-battletech = {
    strategicbattletech.jsActor,
    strategicbattletech.jsItem
  };

  /**
   * Set an initiative formula for the system
   * @type {String}
   */
  CONFIG.Combat.initiative = {
    formula: "2d6",
    decimals: 0
  };

  // Define custom Entity classes
  CONFIG.Actor.entityClass = strategicbattletech.jsActor;
  CONFIG.Item.entityClass = strategicbattletech.jsItem;

  // Register sheet application classes
  Actors.unregisterSheet("core", ActorSheet);
  Actors.registerSheet("strategic-battletech", strategicbattletech.jsActorSheet, { makeDefault: true });
  Items.unregisterSheet("core", ItemSheet);
  Items.registerSheet("strategic-battletech", strategicbattletech.jsItemSheet, { makeDefault: true });

  // If you need to add Handlebars helpers, here are a few useful examples:
  Handlebars.registerHelper('concat', function() {
    var outStr = '';
    for (var arg in arguments) {
      if (typeof arguments[arg] != 'object') {
        outStr += arguments[arg];
      }
    }
    return outStr;
  });

  Handlebars.registerHelper('toLowerCase', function(str) {
    return str.toLowerCase();
  });
});