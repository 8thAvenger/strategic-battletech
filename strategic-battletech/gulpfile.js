const gulp = require('gulp');
const prefix = require('gulp-autoprefixer');
const sourcemaps = require('gulp-sourcemaps');
const less = require('gulp-less');

gulp.task('less', function(cb) {
  gulp
    .src('*.less')
    .pipe(less())
    .pipe(
      gulp.dest(function(f) {
        console.debug(f.base);
        return f.base;
      })
    );
  cb();
});

gulp.task(
  'default',
  gulp.series('less', function(cb) {
    gulp.watch('./less/*.less', gulp.series('less'));
    cb();
  })
);
